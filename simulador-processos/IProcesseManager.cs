﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace simulador_processos {
    public interface IProcesseManager {
        void add(Process p);
        void update(Process p);
    }
}
