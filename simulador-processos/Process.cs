﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace simulador_processos {
    public class Process {

        public int id { get; set; }
        public string name { get; set; }
        public double time_arrive { get; set; }
        public double time { get; set; }
        public int priority { get; set; }
    }
}
