﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace simulador_processos {
    public partial class EditorProcesso : Form {

        IProcesseManager ipm;
        Process process2Edit;

        public EditorProcesso() {
            InitializeComponent();
        }

        public EditorProcesso(IProcesseManager ipm): this() {
            this.ipm = ipm;
            btn_addProcess.Text = "Criar";
        }

        public EditorProcesso(IProcesseManager ipm, Process p)
            : this()
        {
            this.ipm = ipm;
            this.process2Edit = p;

            btn_addProcess.Text = "Atualizar";

            tb_name.Text = p.name;
            tb_processtime.Text = p.time.ToString();
        }

        private void btn_addProcess_Click(object sender, EventArgs e) {
            Process p = new Process();

            p.name = tb_name.Text;
            p.time = double.Parse(tb_processtime.Text);

            if(process2Edit != null) {
                p.id = this.process2Edit.id;
                this.ipm.update(p);
            }
            else
                this.ipm.add(p);
        }
    }
}
