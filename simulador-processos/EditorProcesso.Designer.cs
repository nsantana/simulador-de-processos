﻿namespace simulador_processos {
    partial class EditorProcesso {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_processtime = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tb_name = new System.Windows.Forms.TextBox();
            this.btn_addProcess = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.comboBox1);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.tb_processtime);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.tb_name);
            this.panel1.Controls.Add(this.btn_addProcess);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(260, 247);
            this.panel1.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Tempo do processo";
            // 
            // tb_processtime
            // 
            this.tb_processtime.Location = new System.Drawing.Point(130, 40);
            this.tb_processtime.Name = "tb_processtime";
            this.tb_processtime.Size = new System.Drawing.Size(127, 20);
            this.tb_processtime.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Nome do Processo";
            // 
            // tb_name
            // 
            this.tb_name.Location = new System.Drawing.Point(130, 7);
            this.tb_name.Name = "tb_name";
            this.tb_name.Size = new System.Drawing.Size(127, 20);
            this.tb_name.TabIndex = 3;
            // 
            // btn_addProcess
            // 
            this.btn_addProcess.Location = new System.Drawing.Point(6, 215);
            this.btn_addProcess.Name = "btn_addProcess";
            this.btn_addProcess.Size = new System.Drawing.Size(75, 23);
            this.btn_addProcess.TabIndex = 2;
            this.btn_addProcess.Text = "Adicionar";
            this.btn_addProcess.UseVisualStyleBackColor = true;
            this.btn_addProcess.Click += new System.EventHandler(this.btn_addProcess_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(175, 215);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(82, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "Cancelar";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Prioridade";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Alta",
            "acima normal",
            "normal",
            "abaixo normal",
            "baixa"});
            this.comboBox1.Location = new System.Drawing.Point(130, 75);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(127, 21);
            this.comboBox1.TabIndex = 8;
            // 
            // EditorProcesso
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.panel1);
            this.Name = "EditorProcesso";
            this.Text = "process_editor";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb_processtime;
        private System.Windows.Forms.TextBox tb_name;
        private System.Windows.Forms.Button btn_addProcess;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBox1;

    }
}