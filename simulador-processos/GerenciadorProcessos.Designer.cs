﻿namespace simulador_processos {
    partial class GerenciadorProcessos {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.panel1 = new System.Windows.Forms.Panel();
            this.DGV_Process = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CPU = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Prioridade = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_windowTime = new System.Windows.Forms.TextBox();
            this.lb_time = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.CB_algoritmos = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_start = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.adicionarProcessoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removerProcessoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_Process)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.DGV_Process);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(793, 268);
            this.panel1.TabIndex = 1;
            // 
            // DGV_Process
            // 
            this.DGV_Process.AllowUserToResizeColumns = false;
            this.DGV_Process.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DGV_Process.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.DGV_Process.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.name,
            this.id,
            this.CPU,
            this.time,
            this.Prioridade});
            this.DGV_Process.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DGV_Process.Location = new System.Drawing.Point(0, 0);
            this.DGV_Process.MultiSelect = false;
            this.DGV_Process.Name = "DGV_Process";
            this.DGV_Process.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGV_Process.Size = new System.Drawing.Size(793, 268);
            this.DGV_Process.TabIndex = 0;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Selecionar";
            this.Column1.Name = "Column1";
            this.Column1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Column1.Visible = false;
            // 
            // name
            // 
            this.name.DataPropertyName = "name";
            this.name.HeaderText = "Nome";
            this.name.Name = "name";
            this.name.Visible = false;
            // 
            // id
            // 
            this.id.DataPropertyName = "id";
            this.id.HeaderText = "PID";
            this.id.Name = "id";
            this.id.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // CPU
            // 
            this.CPU.DataPropertyName = "time_arrive";
            this.CPU.HeaderText = "Tempo Chegada";
            this.CPU.Name = "CPU";
            // 
            // time
            // 
            this.time.DataPropertyName = "time";
            this.time.HeaderText = "Tempo";
            this.time.Name = "time";
            // 
            // Prioridade
            // 
            this.Prioridade.DataPropertyName = "priority";
            this.Prioridade.HeaderText = "Prioridade";
            this.Prioridade.Name = "Prioridade";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 24);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 74.93188F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.06812F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(799, 367);
            this.tableLayoutPanel1.TabIndex = 5;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.txt_windowTime);
            this.panel2.Controls.Add(this.lb_time);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.CB_algoritmos);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.btn_start);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 277);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(793, 87);
            this.panel2.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(515, 5);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "milisegundos";
            this.label4.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(363, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Janela de tempo";
            this.label3.Visible = false;
            // 
            // txt_windowTime
            // 
            this.txt_windowTime.Location = new System.Drawing.Point(447, 2);
            this.txt_windowTime.Name = "txt_windowTime";
            this.txt_windowTime.Size = new System.Drawing.Size(62, 20);
            this.txt_windowTime.TabIndex = 10;
            this.txt_windowTime.Visible = false;
            // 
            // lb_time
            // 
            this.lb_time.AutoSize = true;
            this.lb_time.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_time.Location = new System.Drawing.Point(469, 41);
            this.lb_time.Name = "lb_time";
            this.lb_time.Size = new System.Drawing.Size(0, 20);
            this.lb_time.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(323, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(126, 18);
            this.label2.TabIndex = 8;
            this.label2.Text = "Tempo decorrido:";
            // 
            // CB_algoritmos
            // 
            this.CB_algoritmos.FormattingEnabled = true;
            this.CB_algoritmos.Items.AddRange(new object[] {
            "First Come, First Served",
            "Shortest Job First - SJF",
            "Round-Robin - RR"});
            this.CB_algoritmos.Location = new System.Drawing.Point(153, 2);
            this.CB_algoritmos.Name = "CB_algoritmos";
            this.CB_algoritmos.Size = new System.Drawing.Size(177, 21);
            this.CB_algoritmos.TabIndex = 7;
            this.CB_algoritmos.SelectedIndexChanged += new System.EventHandler(this.CB_algoritmos_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(143, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Algoritmo de escalonamento:";
            // 
            // btn_start
            // 
            this.btn_start.Location = new System.Drawing.Point(625, 0);
            this.btn_start.Name = "btn_start";
            this.btn_start.Size = new System.Drawing.Size(75, 23);
            this.btn_start.TabIndex = 5;
            this.btn_start.Text = "Iniciar";
            this.btn_start.UseVisualStyleBackColor = true;
            this.btn_start.Click += new System.EventHandler(this.btn_start_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.adicionarProcessoToolStripMenuItem,
            this.removerProcessoToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(799, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // adicionarProcessoToolStripMenuItem
            // 
            this.adicionarProcessoToolStripMenuItem.Name = "adicionarProcessoToolStripMenuItem";
            this.adicionarProcessoToolStripMenuItem.Size = new System.Drawing.Size(120, 20);
            this.adicionarProcessoToolStripMenuItem.Text = "Adicionar Processo";
            this.adicionarProcessoToolStripMenuItem.Click += new System.EventHandler(this.adicionarProcessoToolStripMenuItem_Click);
            // 
            // removerProcessoToolStripMenuItem
            // 
            this.removerProcessoToolStripMenuItem.Name = "removerProcessoToolStripMenuItem";
            this.removerProcessoToolStripMenuItem.Size = new System.Drawing.Size(116, 20);
            this.removerProcessoToolStripMenuItem.Text = "Remover Processo";
            this.removerProcessoToolStripMenuItem.Click += new System.EventHandler(this.removerProcessoToolStripMenuItem_Click);
            // 
            // GerenciadorProcessos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(799, 391);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "GerenciadorProcessos";
            this.Text = "Simulador de Processos";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DGV_Process)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn name;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn CPU;
        private System.Windows.Forms.DataGridViewTextBoxColumn time;
        private System.Windows.Forms.DataGridViewTextBoxColumn priority;
        private System.Windows.Forms.DataGridView DGV_Process;
        private System.Windows.Forms.DataGridViewTextBoxColumn Prioridade;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem adicionarProcessoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removerProcessoToolStripMenuItem;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txt_windowTime;
        private System.Windows.Forms.Label lb_time;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox CB_algoritmos;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_start;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}

