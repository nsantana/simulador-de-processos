﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;

namespace simulador_processos {
    public partial class GerenciadorProcessos : Form, IProcesseManager {

        List<Process> processes = new List<Process>();

        public GerenciadorProcessos() {
            InitializeComponent();

            DGV_Process.AutoGenerateColumns = false;
        }

        private void btn_addprocess_Click(object sender, EventArgs e) {
            
        }

        private void refresh_table()
        {
            DGV_Process.DataSource = null;
            DGV_Process.DataSource = this.processes;
        }

        public void add(Process p) {
            processes.Add(p);
            p.id = processes.Count;
            this.refresh_table();
        }

        public void update(Process pUpdated)
        {
            int p_index = processes.FindIndex(i => i.id == pUpdated.id);
            processes[p_index] = pUpdated;

            this.refresh_table();
        }

        private void setRowColor(int index, Color c) {
            DGV_Process.Rows[index].DefaultCellStyle.BackColor = c;

        }

        private void doAnimationNoPreemptive(List<Process> tmp_processes, GerenciadorProcessos form_gp) {
            Stopwatch stopwatch = Stopwatch.StartNew();

            for(int i = 0; i < tmp_processes.Count; i++) {
                Process p = tmp_processes[i];
                int index = p.id - 1;

                this.setRowColor(index, Color.Green);

                Thread.Sleep((int)p.time);

                this.setRowColor(index, Color.White);
            }

            form_gp.Invoke((MethodInvoker)delegate() {
                form_gp.lb_time.Text = stopwatch.ElapsedMilliseconds.ToString() + "  milisegundos";
            });

        }

        private void SJF(object form) {

            List<Process> tmp_processes = processes.ToList();

            tmp_processes.Sort((x, y) => {
                return x.time.CompareTo(y.time);
            });

            this.doAnimationNoPreemptive(tmp_processes, (GerenciadorProcessos)form);
        }

        private void FCFS(object form) {

            List<Process> tmp_processes = processes.ToList();

            tmp_processes.Sort((x, y) => {
                return x.time_arrive.CompareTo(y.time_arrive);
            });

            this.doAnimationNoPreemptive(tmp_processes, (GerenciadorProcessos)form);

        }

        private void RR(object form) {

            List<Process> tmp_processes = processes.ToList();

            tmp_processes.Sort((x, y) => {
                return x.time_arrive.CompareTo(y.time_arrive);
            });

            GerenciadorProcessos form_gp = (GerenciadorProcessos)form;
            
            int janela_tempo = 0;
            form_gp.Invoke((MethodInvoker)delegate() {
                janela_tempo = int.Parse(form_gp.txt_windowTime.Text);
            });

            bool naoTerminou = true ;
            int contadorProcessosFinalizados = 0;
            
            Stopwatch stopwatch = Stopwatch.StartNew();

            while(naoTerminou == true) {
                for(int i = 0; i < tmp_processes.Count; i++) {
                    Process p = tmp_processes[i];
                    if(p.time <= 0){
                        contadorProcessosFinalizados += 1;
                        continue;
                    }
                    
                    int index = p.id - 1;
                    p.time = p.time - janela_tempo;

                    this.setRowColor(index, Color.Green);

                    Thread.Sleep(janela_tempo);

                    this.setRowColor(index, Color.White);
                }
                if(contadorProcessosFinalizados == tmp_processes.Count) {
                    naoTerminou = false;
                }
                
                contadorProcessosFinalizados = 0;
            }

            form_gp.Invoke((MethodInvoker)delegate() {
                form_gp.lb_time.Text = stopwatch.ElapsedMilliseconds.ToString() + "  milisegundos";
            });

        }

        private void btn_start_Click(object sender, EventArgs e) {

            int index = CB_algoritmos.SelectedIndex;
             
            if(index == 0) {
                Thread t = new Thread(new ParameterizedThreadStart(FCFS));

                t.Start(this);
            } else if(index == 1) {
                Thread t = new Thread(new ParameterizedThreadStart(SJF));

                t.Start(this);
            } else if(index == 2) {
                Thread t = new Thread(new ParameterizedThreadStart(RR));

                t.Start(this);
            }

        }

        private void adicionarProcessoToolStripMenuItem_Click(object sender, EventArgs e) {
            Process p = new Process();
            p.id = processes.Count() + 1;
            processes.Add(p);

            this.refresh_table();
        }

        private void removerProcessoToolStripMenuItem_Click(object sender, EventArgs e) {

            if(DGV_Process.CurrentRow != null && DGV_Process.CurrentRow.Selected) {
                int id_process = DGV_Process.CurrentRow.Index;
                processes.RemoveAt(id_process);
                this.refresh_table();
            }
        }

        private void CB_algoritmos_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CB_algoritmos.SelectedIndex == 2)
            {
                label3.Visible = true;
                txt_windowTime.Visible = true;
                label4.Visible = true;
            }else
            {
                label3.Visible = false;
                txt_windowTime.Visible = false;
                label4.Visible = false;
            }

        }
    }
}
